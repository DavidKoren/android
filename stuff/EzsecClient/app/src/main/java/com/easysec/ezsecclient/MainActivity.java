package com.easysec.ezsecclient;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
//import android.net.VpnService;
import android.content.Intent;
import android.view.View;
import android.widget.Toast;


public class MainActivity extends Activity implements View.OnClickListener {
    private View view;
    private ezsecVpnService myVpnService;
    private Button b = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        TextView text = new TextView(this);
//        text.setText(R.string.app_name);
//        setContentView(text);
        setContentView(R.layout.activity_main);
        b = (Button) findViewById(R.id.connect);
        b.setOnClickListener(this);
    }

    @Override
    public void onClick(View v)
    {
        switch(v.getId()){
            case R.id.connect:
                TextView result = (TextView)findViewById(R.id.result);
                result.setText("Welcome to EasySec?");
                Intent intent = myVpnService.prepare(getApplicationContext());
                if (intent != null) {
                    startActivityForResult(intent, 0);
                } else {
                    onActivityResult(0, RESULT_OK, null);
                }
                break;
        }
/*        Intent intent = VpnService.prepare(getApplicationContext());
        if (intent != null) {
            startActivityForResult(intent, 0);
        } else {
            onActivityResult(0, RESULT_OK, null);
        }
        */
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            EditText username = (EditText)findViewById(R.id.address);
            TextView result = (TextView)findViewById(R.id.result);
            String t = username.getText().toString();
            result.setText("Welcome to EasySec "+ t);
        }
        else
        {
            Toast.makeText(MainActivity.this, "You cannot connect the EasySec System", Toast.LENGTH_LONG).show();
        }
    }
}
